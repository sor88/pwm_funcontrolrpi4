#!/usr/bin/python
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import sys, traceback                       # Импортируем библиотеки для обработки исключений
#import time
from time import sleep                      # Импортируем библиотеку для работы со временем
from re import findall                      # Импортируем библиотеку по работе с регулярными выражениями
from subprocess import check_output         # Импортируем библиотеку по работе с внешними процессами

def get_temp():
    temp = check_output(["vcgencmd","measure_temp"]).decode()    # Выполняем запрос температуры
    temp = float(findall('\d+\.\d+', temp)[0])                   # Извлекаем при помощи регулярного выражения значение температуры из строки "temp=47.8'C"
    return(temp)                            # Возвращаем результат


                          # Set Pi to use pin number when referencing GPIO pins.


try:
#    tempOn = 45                             # Температура включения кулера
#    controlPin = 14                         # Пин отвечающий за управление
    dc = 0
    # === Инициализация пинов ===
    GPIO.setmode(GPIO.BCM)                  # Режим нумерации в BCM
#    GPIO.setup(controlPin, GPIO.OUT, initial=0) # Управляющий пин в режим OUTPUT
#    GPIO.setmode(GPIO.BOARD)  # Can use GPIO.setmode(GPIO.BCM) instead to use
                          # Broadcom SOC channel names.
    GPIO.setup(14, GPIO.OUT)  # Set GPIO pin 14 to output mode.
    pwm = GPIO.PWM(14, 500)   # Initialize PWM on pwmPin 100Hz frequency

    #pwm.start(dc)                      # Start PWM with 0% duty cycle

    while True:
#        pwm.start(9)
        temp = get_temp()
        if temp >= 50 and  temp<65:
                dc = temp / 5
                if dc < 9: dc=9
                pwm.start(dc)
        if temp >= 65:
                pwm.start(100)
                dc = max
        if temp < 40:
                pwm.stop()
                dc = 0
        print(str(temp)+ ' ' + str(dc))
        sleep(5)


except KeyboardInterrupt:
    # ...
    print("Exit pressed Ctrl+C")            # Выход из программы по нажатию Ctrl+C

except:
    # ...
    print("Other Exception")                # Прочие исключения
    print("--- Start Exception Data:")
    traceback.print_exc(limit=2, file=sys.stdout) # Подробности исключения через traceback
    print("--- End Exception Data:")
finally:
    print("CleanUp")                        # Информируем о сбросе пинов
    GPIO.cleanup()                          # Возвращаем пины в исходное состояние
    print("End of program")                 # Информируем о завершении работы программы
